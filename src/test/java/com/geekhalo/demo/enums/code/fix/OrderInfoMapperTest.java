package com.geekhalo.demo.enums.code.fix;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class OrderInfoMapperTest {
    @Autowired
    private OrderInfoMapper orderInfoMapper;

    @Test
    public void save() {
        OrderInfoBO orderInfo = new OrderInfoBO();
        orderInfo.setStatus(CodeBasedOrderStatus.CREATED);
        orderInfoMapper.save(orderInfo);
    }
}
