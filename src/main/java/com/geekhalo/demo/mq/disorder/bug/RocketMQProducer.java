package com.geekhalo.demo.mq.disorder.bug;

import com.geekhalo.demo.mq.disorder.OrderCreatedEvent;
import com.geekhalo.demo.mq.event.fix.OrderPaidEvent;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
public class RocketMQProducer {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @TransactionalEventListener
    public void handle(OrderCreatedEvent event){
        rocketMQTemplate.convertAndSend("order_created_event", event);
    }

    @TransactionalEventListener
    public void handle(OrderPaidEvent event){
        rocketMQTemplate.convertAndSend("order_paid_event", event);
    }
}