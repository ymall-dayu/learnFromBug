package com.geekhalo.demo.mq.event;

import lombok.Data;

@Data
public class Order {
    private Long id;

    public Order(Long id) {
        setId(id);
    }

    public void paySuccess() {

    }
}
