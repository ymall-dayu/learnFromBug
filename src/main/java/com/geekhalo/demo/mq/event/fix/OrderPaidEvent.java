package com.geekhalo.demo.mq.event.fix;

import com.geekhalo.demo.mq.event.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderPaidEvent {
    private Order order;

    public Long getOrderId() {
        return null;
    }
}
