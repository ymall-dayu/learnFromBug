package com.geekhalo.demo.mq.sender.bug;

import com.geekhalo.demo.mq.sender.OrderPaidEvent;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
public class RocketMQProducer {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @EventListener
    public void handle(OrderPaidEvent event){
        rocketMQTemplate.convertAndSend("order_event", event);
    }
}
