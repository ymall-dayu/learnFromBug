package com.geekhalo.demo.mq.sender.fix;

import com.geekhalo.demo.mq.event.Order;
import com.geekhalo.demo.mq.event.fix.OrderPaidEvent;
import com.geekhalo.demo.mq.sender.OrderRepository;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.transaction.Transactional;

public class OrderService2 {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Transactional
    public void paySuccess(String orderId, String token){
        // 验证 token，保障有效性
        checkToke(token);

        // 加载订单信息
        Order order = this.orderRepository.getById(orderId);
        if (order == null){
            throw new RuntimeException("订单不存在");
        }
        // 支付成功，更新订单状态
        order.paySuccess();
        // 将变更更新到数据库
        this.orderRepository.update(order);

        this.eventPublisher.publishEvent(new OrderPaidEvent(order));

        doSomething();
    }

    private void doIfCommitted(Runnable task) {
        if (TransactionSynchronizationManager.isSynchronizationActive()) {
            TransactionSynchronization transactionSynchronization = new TransactionSynchronizationAdapter(){
                @Override
                public void afterCommit() {
                    task.run();
                }
            };
            TransactionSynchronizationManager.registerSynchronization(transactionSynchronization);

        }else {
            task.run();
        }
    }

    private void doSomething() {

    }


    private void checkToke(String token) {

    }
}
