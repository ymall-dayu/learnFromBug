package com.geekhalo.demo.mq.sender.bug;

import com.geekhalo.demo.mq.event.Order;
import com.geekhalo.demo.mq.event.fix.OrderPaidEvent;
import com.geekhalo.demo.mq.sender.OrderRepository;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

import javax.transaction.Transactional;

public class OrderService {
    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private OrderRepository orderRepository;

    @Transactional
    public void paySuccess(String orderId, String token){
        // 验证 token，保障有效性
        checkToke(token);

        // 加载订单信息
        Order order = this.orderRepository.getById(orderId);
        if (order == null){
            throw new RuntimeException("订单不存在");
        }
        // 支付成功，更新订单状态
        order.paySuccess();

        // 将变更更新到数据库
        this.orderRepository.update(order);

        // 发送支付成功事件
        this.eventPublisher.publishEvent(new OrderPaidEvent(order));
        // 执行其他业务逻辑
        doSomething();
    }

    private void doSomething() {

    }


    private void checkToke(String token) {

    }
}
