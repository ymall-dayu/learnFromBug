package com.geekhalo.demo.thread.parallelfun;

import java.util.stream.Stream;

public enum ProductType {
    BOOK(1),
    CLAZZ(2),
    COUPON(3);

    private final int code;

    ProductType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static ProductType parallelParseByCode(int code) {
        return Stream.of(values())
                .parallel()
                .filter(type -> type.getCode() == code)
                .findFirst()
                .orElse(null);
    }

    public static ProductType parseByCode(int code) {
        return Stream.of(values())
                .filter(type -> type.getCode() == code)
                .findFirst()
                .orElse(null);
    }
}
