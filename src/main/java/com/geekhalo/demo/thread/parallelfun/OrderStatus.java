package com.geekhalo.demo.thread.parallelfun;

import com.geekhalo.demo.enums.code.fix.CodeBasedOrderStatus;

import java.util.stream.Stream;

public enum OrderStatus {
    CREATED(1),
    TIMEOUT_CANCELLED(2),
    MANUAL_CANCELLED(5),
    PAID(3),
    FINISHED(4);
    private final int code;

    OrderStatus(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public static OrderStatus parallelParseByCode(int code) {
        return Stream.of(values())
                .parallel()
                .filter(status -> status.getCode() == code)
                .findFirst()
                .orElse(null);
    }

    public static OrderStatus parseByCode(int code) {
        return Stream.of(values())
                // .parallel() 直接使用串行执行
                .filter(status -> status.getCode() == code)
                .findFirst()
                .orElse(null);
    }
}
