package com.geekhalo.demo.thread.paralleltask;

import com.geekhalo.demo.util.SleepUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    public User getById(Long id){
        SleepUtil.sleep(1);
        return new User(id, "小明");
    }

    @Data
    @AllArgsConstructor
    public static class User{
        private Long id;
        private String name;
    }
}
