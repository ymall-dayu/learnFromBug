package com.geekhalo.demo.thread.paralleltask;

import lombok.Data;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    public Order getById(Long orderId) {

        Order order = new Order();
        order.setId(orderId);
        order.setUserId(orderId + 1);
        order.setCouponId(orderId + 2);
        order.setProductId(orderId + 3);
        order.setUserAddressId(orderId + 4);
        return order;
    }


    @Data
    public static class Order{
        private Long id;
        private Long userId;
        private Long userAddressId;
        private Long productId;
        private Long couponId;
    }
}
