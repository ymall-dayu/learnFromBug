package com.geekhalo.demo.thread.paralleltask;

import lombok.Data;

@Data
public class OrderDetailVO {
    private AddressService.Address address;
    private UserService.User user;
    private CouponService.Coupon coupon;
    private ProductService.Product product;
}
