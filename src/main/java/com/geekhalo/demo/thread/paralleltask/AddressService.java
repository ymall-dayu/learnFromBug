package com.geekhalo.demo.thread.paralleltask;

import com.geekhalo.demo.util.SleepUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

@Service
public class AddressService {

    public Address getById(Long id) {
        SleepUtil.sleep(1);
        return new Address(id, "北京-海淀-中关村");
    }

    @Data
    @AllArgsConstructor
    public static class Address{
        private Long id;
        private String address;
    }
}
