package com.geekhalo.demo.enums.support;

import lombok.Data;

@Data
public class OrderVO {
    private Long id;
    private CommonOrderStatus status;
}
