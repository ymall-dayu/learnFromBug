package com.geekhalo.demo.enums.support;

import com.geekhalo.lego.core.enums.repository.mybatis.CommonEnumTypeHandler;
import org.apache.ibatis.type.MappedTypes;

@MappedTypes(CommonOrderStatus.class)
public class CommonOrderStatusTypeHandler extends CommonEnumTypeHandler<CommonOrderStatus> {
    public CommonOrderStatusTypeHandler() {
        super(CommonOrderStatus.values());
    }
}
