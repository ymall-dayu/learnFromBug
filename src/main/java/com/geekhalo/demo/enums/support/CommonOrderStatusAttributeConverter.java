package com.geekhalo.demo.enums.support;

import com.geekhalo.lego.core.enums.repository.jpa.CommonEnumAttributeConverter;

import javax.persistence.Converter;

@Converter(autoApply = true)
public class CommonOrderStatusAttributeConverter extends CommonEnumAttributeConverter<CommonOrderStatus> {
    public CommonOrderStatusAttributeConverter() {
        super(CommonOrderStatus.values());
    }
}
