package com.geekhalo.demo.enums.descr.fix;

import lombok.Data;

@Data
public class OrderVO {
    private Long id;
    private SelfDescribedEnumBasedOrderStatus status;
}
