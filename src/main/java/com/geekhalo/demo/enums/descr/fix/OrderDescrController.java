package com.geekhalo.demo.enums.descr.fix;

import org.apache.commons.lang3.RandomUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@RestController("DescrFixController")
@RequestMapping("enums/descr/fix")
public class OrderDescrController {

    @GetMapping("myOrders")
    public List<OrderVO> myOrders(){
        return LongStream.range(0, 5)
                .mapToObj( id ->{
                    OrderVO order = new OrderVO();
                    order.setId(id);
                    order.setStatus(createRandomStatus());
                    return order;
                }).collect(Collectors.toList());
    }

    private SelfDescribedEnumBasedOrderStatus createRandomStatus() {
        int length = SelfDescribedEnumBasedOrderStatus.values().length;
        int index = RandomUtils.nextInt(0, length);
        return SelfDescribedEnumBasedOrderStatus.values()[index];
    }
}
