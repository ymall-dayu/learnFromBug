package com.geekhalo.demo.enums.code.fix;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class CodeBasedOrderStatusConverter implements AttributeConverter<CodeBasedOrderStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(CodeBasedOrderStatus e) {
        return e.getCode();
    }

    @Override
    public CodeBasedOrderStatus convertToEntityAttribute(Integer code) {
        for (CodeBasedOrderStatus e : CodeBasedOrderStatus.values()) {
            if (e.getCode() == code) {
                return e;
            }
        }
        return null;
    }
}
