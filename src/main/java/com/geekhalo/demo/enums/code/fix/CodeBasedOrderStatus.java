package com.geekhalo.demo.enums.code.fix;

import com.geekhalo.lego.common.enums.CodeBasedEnum;
import lombok.Getter;

@Getter
public enum CodeBasedOrderStatus implements CodeBasedEnum{
	CREATED(1),
	TIMEOUT_CANCELLED(2),
	MANUAL_CANCELLED(5),
	PAID(3),
	FINISHED(4);
	private final int code;

	CodeBasedOrderStatus(int code) {
		this.code = code;
	}

	public static void main(String... args){
		for (CodeBasedOrderStatus orderStatus : CodeBasedOrderStatus.values()){
			System.out.println(orderStatus.name() + ":" + orderStatus.getCode());
		}
	}
}