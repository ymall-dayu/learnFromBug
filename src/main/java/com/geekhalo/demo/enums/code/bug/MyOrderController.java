package com.geekhalo.demo.enums.code.bug;

import com.geekhalo.demo.enums.code.OrderVO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("enums/code/bug")
@RestController(value = "MyBugOrderController")
public class MyOrderController {
    @GetMapping("myOrders")
    public List<OrderVO> myOrders(@RequestParam("status") OrderStatus status) {
        // 忽略逻辑
        return null;
    }
}
