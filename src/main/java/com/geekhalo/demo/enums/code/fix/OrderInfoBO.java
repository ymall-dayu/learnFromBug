package com.geekhalo.demo.enums.code.fix;

import lombok.Data;

@Data
public class OrderInfoBO {
    private Long id;
    private CodeBasedOrderStatus status;
}
